#!/bin/bash

# if lower_bound*upper_bound > 0, then we consider the benchmark "changed"
NOISE=0.02
JQ_FILTER="if .Median.confidence_interval.lower_bound > $NOISE or .Median.confidence_interval.upper_bound < -$NOISE then .Median.point_estimate else \"\" end"

ESTIMATES=$(find target -path \*change/estimates.json -print0 \
    | xargs -0 cat| jq -r "$JQ_FILTER" | sed '/^$/d' | sort -g)

better=0
worse=0

for estimate in $ESTIMATES; do
    if [[ "$estimate" == "-"* ]]; then
        echo $estimate better
        better=$((better+1))
    else
        echo $estimate worse
        worse=$((worse+1))
    fi
done

DESC="Improved: $better, regressed: $worse"

if [[ "$better" == "0" && "$worse" == "0" ]]; then
    DESC="No changes in benchmarks"
fi

STATE="failed"

if [[ "$worse" == "0" ]]; then
    STATE="success"
fi

URL="https://www.rubdos.be/embroidery/embroidery-formats-rs/criterion/$CI_COMMIT_REF_SLUG/"

curl --request POST \
     --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
     -F description="$DESC" \
     -F state="$STATE" \
     -F name="Benchmarks" \
     -F target_url="$URL" \
     "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/statuses/$CI_COMMIT_SHA"
