use std::fs::File;

use criterion::*;
use dst::{DstReader, parse, parse_header};

fn bench_parse_header(c: &mut Criterion) {
    c.bench("parse_header", Benchmark::new("parse_header", |b| {
        let vector = include_bytes!("../src/test-vectors/header.dst");
        let vector = &vector[..];

        b.iter(|| {
            let header = parse_header(vector);
            let (trailing, header) = header.expect("Invalid header");

            assert_eq!(trailing, &b""[..]);

            assert_eq!(header["LA"], "Star.DST");
            assert_eq!(header["ST"], "2567");
        });
    }).throughput(Throughput::Bytes(512)));
}

fn bench_parse_file_in_memory(c: &mut Criterion) {
    let vector = include_bytes!("../src/test-vectors/star.dst");
    c.bench("parse-file-in-memory", Benchmark::new("parse-file-in-memory", move |b| {
        let vector = &vector[..];

        b.iter(|| {
            let parsed = parse(vector);
            let (trailing, (header, _pattern)) = parsed.expect("Invalid header");

            assert_eq!(trailing, &b""[..]);

            assert_eq!(header["LA"], "Star.DST");
            assert_eq!(header["ST"], "2567");
        });
    }).throughput(Throughput::Bytes(vector.len() as u32)));
}

fn bench_parse_file_streaming(c: &mut Criterion) {
    let vector = include_bytes!("../src/test-vectors/star.dst");
    let len = vector.len();
    c.bench("parse-file-streaming", Benchmark::new("parse-file-streaming", move |b| {
        b.iter(|| {
            let f = File::open("src/test-vectors/star.dst").unwrap();
            let mut reader = DstReader::new(f);

            let header = reader.read_header();
            reader.last()
        });
    }).throughput(Throughput::Bytes(len as u32)));
}

criterion_group!(parse_dst,
                 bench_parse_header,
                 bench_parse_file_streaming,
                 bench_parse_file_in_memory,
);
criterion_main!(parse_dst);
