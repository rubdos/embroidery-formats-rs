use std::io::Read;
use std::ops::{Range, RangeFrom, RangeTo};

use nom::*;
use nom::types::CompleteByteSlice;

use crate::format::*;

// TODO: verbose errors needs Context::List too.
macro_rules! ctx_transpose {
    ($i:ident) => {
        match $i {
            Context::Code(a, b) => Context::Code(T::from(a.0), b),
        }
    }
}

pub trait IntoBytes<'a> {
    fn into_bytes(self) -> &'a [u8];
}

impl<'a> IntoBytes<'a> for &'a [u8] {
    fn into_bytes(self) -> &'a [u8] {
        self
    }
}

impl<'a> IntoBytes<'a> for CompleteByteSlice<'a> {
    fn into_bytes(self) -> &'a [u8] {
        self.0
    }
}

const CHUNK: usize = 4096;

pub struct DstReader<R> {
    source: R,
    buffer: Vec<u8>,
    pos: usize,
    header_read: bool,
    prev: Stitch,
}

impl<R> DstReader<R> {
    pub fn new(r: R) -> Self {
        Self::new_with_capacity(r, CHUNK)
    }

    pub fn new_with_capacity(r: R, capacity: usize) -> Self {
        DstReader {
            source: r,
            buffer: vec![0; capacity],
            pos: 0,
            header_read: false,
            prev: Stitch {
                jump: false,
                color_change: false,
                x: 0,
                y: 0,
            },
        }
    }
}

impl<R: Read> DstReader<R> {
    pub fn read_header<'a>(&'a mut self)
        -> Header<&'a str, &'a str>
    {
        debug_assert!(!self.header_read, "Header already read");
        let read = self.source.read(&mut self.buffer).unwrap();
        assert!(read >= 512 + 3, "truncated file");

        let (_rest, header) = parse_header(&self.buffer[..]).unwrap();

        self.header_read = true;
        self.pos += 512;

        header
    }

    pub fn maybe_need_more(&mut self) {
        if self.pos + 3 > self.buffer.len() {
            let (left, right) = self.buffer.split_at_mut(self.pos);
            assert!(left.len() > right.len());
            left[..right.len()].copy_from_slice(right);

            self.pos = 0;
            let valid_length = right.len();

            let read = self.source.read(&mut self.buffer[valid_length..]).unwrap();

            if read + valid_length < 4096 {
                self.buffer.truncate(read + valid_length);
            }
        }
    }
}

impl<R: Read> Iterator for DstReader<R> {
    type Item = Stitch;

    fn next(&mut self) -> Option<Stitch> {
        debug_assert!(self.header_read, "Header not yet read");
        self.maybe_need_more();

        if self.buffer.len() < 3 {
            debug_assert_eq!(self.buffer.len(), 0,
                "Truncated DST file");
            return None;
        }

        match tag!(&self.buffer[self.pos..], FINAL_TRIPLET) {
            Ok((_rest, _tag)) => {
                None
            },
            Err(Err::Error(_)) => {
                match relative_stitch(&self.buffer[self.pos..]) {
                    Ok((_rest, rel)) => {
                        self.pos += 3;
                        let stitch = rel.to_stitch(&self.prev);
                        self.prev = stitch.clone();
                        Some(stitch)
                    }
                    Err(e) => panic!("Error parsing stitch {:?}", e),
                }
            }
            Err(e) => panic!("Error parsing tag: {:?}", e)
        }
    }
}

/// Parses a DST file.
/// Input should be &[u8] or CompleteByteSlice<'a>.
///
/// ```
/// # extern crate dst;
/// let vector = include_bytes!("test-vectors/star.dst");
/// let vector = &vector[..];
/// let (rest, (header, star)) = dst::parse(vector).unwrap();
/// assert_eq!(rest, &b""[..]);
/// assert_eq!(star.stitches.len(), 2566);
/// ```
pub fn parse<'a, T>(input: T) -> IResult<T, (Header<&'a str, &'a str>, Pattern), u32>
where
  T: 'a,
  T: Slice<Range<usize>> + Slice<RangeFrom<usize>> + Slice<RangeTo<usize>>,
  T: Clone + Copy + Offset,
  T: Compare<&'a [u8]>,
  T: AsBytes + InputIter + AtEof,
  T: PartialEq,
  T: FindSubstring<&'a [u8]>,
  <T as InputIter>::Item: AsChar,
  T: InputTake + InputTakeAtPosition,
  T: From<&'a [u8]>,
  T: IntoBytes<'a>,
  <T as InputTakeAtPosition>::Item: AsChar
{
    let (mut input, header) = parse_header(input)?;

    let mut stitches = Vec::with_capacity(input.as_bytes().len()/3);

    let mut prev = &Stitch {
        jump: false,
        color_change: false,
        x: 0,
        y: 0,
    };

    let input: T = loop {
        match tag!(input.clone(), FINAL_TRIPLET) {
            Ok((rest, _end)) => break rest,
            Err(Err::Error(_)) => (),
            Err(e) => Err(e)?,
        }
        let (rest, rel) = relative_stitch(input)?;
        let stitch = rel.to_stitch(prev);
        input = rest;
        stitches.push(stitch);
        prev = &stitches[stitches.len() - 1];
    };

    let pattern = Pattern {
        stitches,
    };

    Ok((input, (header, pattern)))
}

pub fn parse_header<'a, T>(input: T) -> IResult<T, Header<&'a str, &'a str>, u32>
where
  T: 'a,
  T: Slice<Range<usize>> + Slice<RangeFrom<usize>> + Slice<RangeTo<usize>>,
  T: Clone + Copy + Offset,
  T: AsBytes + InputIter + AtEof,
  T: PartialEq,
  T: FindSubstring<&'a [u8]>,
  <T as InputIter>::Item: AsChar,
  T: InputTake + InputTakeAtPosition,
  T: From<&'a [u8]>,
  T: IntoBytes<'a>,
  <T as InputTakeAtPosition>::Item: AsChar
{
    let (rest, header_in) = take!(input, 512)?;
    let header_in = CompleteByteSlice(header_in.into_bytes());

    match many0!(header_in, header_field) {
        // malformed header
        Err(Err::Incomplete(e)) => Err(Err::Incomplete(e)),
        Err(Err::Error(ctx)) => Err(Err::Error(ctx_transpose!(ctx))),
        Err(Err::Failure(ctx)) => Err(Err::Failure(ctx_transpose!(ctx))),
        Ok((_trailing, fields)) => {
             Ok((rest, Header::from_fields(fields)))
        }
    }
}

fn relative_stitch<T>(input: T) -> IResult<T, RelativeStitch, u32>
where
  T: Slice<Range<usize>> + Slice<RangeFrom<usize>> + Slice<RangeTo<usize>>,
  T: Clone + Copy + Offset,
  T: AsBytes + InputIter + AtEof,
  T: PartialEq,
  <T as InputIter>::Item: AsChar,
  T: InputTake + InputTakeAtPosition,
  <T as InputTakeAtPosition>::Item: AsChar
{
    do_parse!(input,
        b0: take!(1) >>
        b1: take!(1) >>
        b2: take!(1) >>
        (RelativeStitch::from_bytes(b0.as_bytes()[0], b1.as_bytes()[0], b2.as_bytes()[0]))
    )
}

fn header_field<'a, T>(input: T) -> IResult<T, (&'a str, &'a str), u32>
where
  T: 'a,
  T: Slice<Range<usize>> + Slice<RangeFrom<usize>> + Slice<RangeTo<usize>>,
  T: Clone + Copy + Offset,
  T: AsBytes + InputIter + AtEof,
  T: PartialEq,
  T: FindSubstring<&'a [u8]>,
  <T as InputIter>::Item: AsChar,
  T: InputTakeAtPosition,
  T: IntoBytes<'a>,
  <T as InputTakeAtPosition>::Item: AsChar
{
    do_parse!(input,
        key: map_res!(map!(take_until_and_consume!(b":" as &[u8]),
                           IntoBytes::into_bytes),
                           std::str::from_utf8) >>
        val: map_res!(map!(take_until_and_consume!(b"\r" as &[u8]),
                           IntoBytes::into_bytes),
                           std::str::from_utf8) >>
        (key, val.trim())
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_header_field() {
        let vector = &b"abc:def\r"[..];
        let expected = ("abc", "def");

        assert_eq!(header_field(vector),
                   Ok((b"" as &[u8], expected)));
    }

    #[test]
    fn test_header() {
        let vector = include_bytes!("test-vectors/header.dst");
        let vector = &vector[..];

        let header = parse_header(vector);
        let (trailing, header) = header.expect("Invalid header");

        assert_eq!(trailing, &b""[..]);

        assert_eq!(header["LA"], "Star.DST");
        assert_eq!(header["ST"], "2567");
    }

    #[test]
    fn test_stitch() {
        let vectors = [
            // dx/dy, jump, color change
            (&b"\x00\x00\x83", (0, 0, false, false))
        ];

        for &(vector, expected) in &vectors {
            let (rest, stitch) = relative_stitch(&vector[..]).unwrap();
            assert_eq!(rest, &b""[..]);

            assert_eq!(stitch.dx, expected.0);
            assert_eq!(stitch.dy, expected.1);
            assert_eq!(stitch.jump, expected.2);
            assert_eq!(stitch.color_change, expected.3);
        }
    }

    #[test]
    fn test_star() {
        let vector = include_bytes!("test-vectors/star.dst");
        let vector = &vector[..];
        let (rest, (_header, star)) = parse(vector).unwrap();
        assert_eq!(rest, &b""[..]);
        assert_eq!(star.stitches.len(), 2566);
    }

    #[test]
    fn test_star_with_reader() {
        use std::fs::*;

        let vector = include_bytes!("test-vectors/star.dst");
        let vector = &vector[..];
        let (rest, (_header, star)) = parse(vector).unwrap();
        assert_eq!(rest, &b""[..]);
        assert_eq!(star.stitches.len(), 2566);

        let mut reader = DstReader::new(File::open("src/test-vectors/star.dst").unwrap());
        let _header = reader.read_header();
        let stitches: Vec<_> = reader.collect();
        assert_eq!(star.stitches.len(), stitches.len());
        assert_eq!(star.stitches, stitches);
    }
}
