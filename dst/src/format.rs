use std::collections::HashMap;
use std::borrow::Borrow;
use std::ops::Index;
use std::hash::Hash;
use std::iter::FromIterator;

pub const FINAL_TRIPLET: &'static [u8] = b"\x00\x00\xf3";

pub struct Header<K, S> {
    fields: HashMap<K, S>,
}

impl<S: AsRef<str>, K: AsRef<str> + Eq + Hash> Header<K, S> {
    pub fn from_fields<I>(fields: I) -> Header<K, S>
        where I: IntoIterator<Item=(K, S)>,
    {
        Header {
            fields: HashMap::from_iter(fields),
        }
    }
}

impl<'a, K, S, Q: ?Sized> Index<&'a Q> for Header<K, S>
    where K: Eq + Hash + Borrow<Q>,
          Q: Eq + Hash,
{
    type Output = S;
    fn index(&self, key: &Q) -> &S
    {
        self.fields.index(key)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Stitch {
    pub jump: bool,
    pub color_change: bool,
    pub x: i16,
    pub y: i16,
}

#[derive(Clone, Debug)]
pub struct RelativeStitch {
    pub jump: bool,
    pub color_change: bool,
    pub dx: i16,
    pub dy: i16,
}

const STITCH_DECODING_B0: &'static[i16] = &[
    1, -1, 9, -9,
    -9, 9, -1, 1,
];

const STITCH_DECODING_B1: &'static[i16] = &[
    3, -3, 27, -27,
    -27, 27, -3, 3,
];

const STITCH_DECODING_B2: &'static[i16] = &[
    0, 0, 81, -81,
    -81, 81, 0, 0,
];

impl RelativeStitch {
    pub fn from_bytes(b0: u8, b1: u8, b2: u8) -> RelativeStitch {
        let (mut dx, mut dy) = (0, 0);


        let decoders = &[
            (b0, STITCH_DECODING_B0),
            (b1, STITCH_DECODING_B1),
            (b2, STITCH_DECODING_B2),
        ];

        for (byte, decoder) in decoders {
            for (i, action) in decoder.iter().enumerate() {
                if byte & (1 << i) != 0 {
                    if i < 4 {
                        dx += action;
                    } else {
                        dy += action;
                    }
                }
            }
        }

        debug_assert!(b2 & 1 != 0);
        debug_assert!(b2 & 2 != 0);

        RelativeStitch {
            dx, dy,
            jump: b2 & 64 != 0,
            color_change: b2 & 32 != 0,
        }
    }

    pub fn to_stitch(self, base: &Stitch) -> Stitch {
        let RelativeStitch {
            dx, dy, jump, color_change,
        } = self;

        Stitch {
            jump, color_change,
            x: base.x + dx,
            y: base.y + dy,
        }
    }
}

pub struct Pattern {
    pub stitches: Vec<Stitch>,
}
