use std::fs::*;

use clap::{App, Arg};
use image::ImageBuffer;

use dst::{DstReader, Stitch};

fn main() {
    let matches = App::new("dst-render")
        .version("0.1.0")
        .author("Ruben De Smet <me AT rubdos DOT be>")
        .about("Renders Tajima DST embroidery patterns into PNG images")
        .arg(Arg::with_name("input")
             .required(true)
             .takes_value(true))
        .arg(Arg::with_name("output")
             .required(true)
             .takes_value(true))
        .get_matches();

    let input = matches.value_of("input")
        .unwrap();
    let output = matches.value_of("output")
        .unwrap();

    let f = File::open(input).expect("input file not found");
    let mut reader = DstReader::new(f);
    let header = reader.read_header();

    let mut minx: i16 = header["-X"].parse().unwrap();
    let mut plusx: i16 = header["+X"].parse().unwrap();
    let mut miny: i16 = header["-Y"].parse().unwrap();
    let mut plusy: i16 = header["+Y"].parse().unwrap();

    // If we have a lazy DST generator that doesn't provide the bounds in the header,
    // we have to go over the *whole file* to know the correct bounds.
    // For now, we collect the whole file in memory and reuse it.
    // It a later stage, we might want to re-read the file from disk if it's too large.
    let reader: Box<Iterator<Item=Stitch>> = if minx == 0 && plusx == 0 && miny == 0 && plusy == 0 {
        let collected: Vec<_> = reader.collect();
        for stitch in collected.iter() {
            if stitch.x < minx { minx = stitch.x }
            if stitch.x > plusx { plusx = stitch.x }
            if stitch.y < miny { miny = stitch.y }
            if stitch.y > plusy { plusy = stitch.y }
        }
        Box::new(collected.into_iter())
    } else {
        Box::new(reader)
    };
    minx = minx.abs() + 10;
    miny = miny.abs() + 10;
    plusx += 10;
    plusy += 10;

    let mut img = ImageBuffer::new((minx + plusx) as u32, (miny + plusy) as u32);

    let mut from = Stitch {
        jump: false,
        color_change: false,
        x: minx as i16,
        y: miny as i16,
    };
    for to in reader {
        for x in from.x..to.x {
            let y = to.y + (from.y - to.y)*(x - to.x)/(from.x - to.x);

            let draw_x = x + minx as i16;
            let draw_y = y + miny as i16;
            img.put_pixel(draw_x as u32,
                          img.height() - (draw_y as u32),
                          image::Luma([255 as u8]));
        }

        from = to;
    }

    img.save(output).unwrap();
}
